import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'camelcase'
})
export class CamelcasePipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    var str = value.split(" ");
    for (let i = 0; i < str.length; i++){
      if(str[i].length > 0){
          let nombreAux = str[i].split("");
          nombreAux[0] = nombreAux[0].toUpperCase();
          str[i] = nombreAux.join("");
        }
      
    }
    return str.join(" ");
  }

}