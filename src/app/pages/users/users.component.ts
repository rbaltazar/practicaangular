import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../../services/services.service';
import { User } from '../../models/user';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  users:User[];
  constructor(private services: ServicesService) { }

  ngOnInit() {
    this.services.getUsers().subscribe(users =>{
      this.users = users;
    });
  }

}
