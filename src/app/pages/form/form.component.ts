import { Component, OnInit, Inject } from '@angular/core';
import {  FormGroup, Validators, FormControl } from '@angular/forms'; 
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { UserForm } from '../../models/user-form';
 ;
@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  registerUserForm: FormGroup;
  register: boolean = false;
  users: UserForm[] = [];
  constructor(@Inject(LOCAL_STORAGE) private storage: StorageService) { }

  ngOnInit() {
    this.initForm();
    this.verifyDataInStorage();
  }
  initForm(){
    this.registerUserForm = new FormGroup({
      name: new FormControl(null, [Validators.required, Validators.maxLength(60), Validators.minLength(4)]),
      age: new FormControl(null, [Validators.required, Validators.maxLength(3), Validators.minLength(1), Validators.pattern(new RegExp(/^[1-9]{1,}$/))]),
      email: new FormControl(null, [Validators.required, Validators.email]),
      phone: new FormControl(null, [Validators.required, Validators.minLength(8), Validators.maxLength(20), Validators.pattern(new RegExp(/[1-9]{1}/g))]),
      address: new FormControl(null, [Validators.required, Validators.minLength(5), Validators.maxLength(100)]),
    })
  }
  registerUser(){
    console.log(this.registerUserForm);    
    this.register = true;
    if(this.registerUserForm.invalid)
      return;    
    this.users.push(this.registerUserForm.value)
    this.storage.set('users', this.users)
    this.reset();
    
  }
  reset(){
    this.registerUserForm.reset();
    this.register = false;
  }
  verifyDataInStorage(){
    let users = this.storage.get('users');
    console.log('values storage',users);
    if(users)
      this.users = users;  
  }
}
