import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { FormComponent } from './pages/form/form.component';
import { UsersComponent } from './pages/users/users.component';


const routes: Routes = [
  {path:'', component: HomeComponent},  
  {path:'home', component:HomeComponent},
  {path:'form', component: FormComponent},
  {path:'users', component: UsersComponent},
  {path:'**', redirectTo:''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
