import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
//components
import { NavbarComponent } from './shared/navbar/navbar.component';
import { FooterComponent } from './shared/footer/footer.component';
import { HomeComponent } from './pages/home/home.component';
import { FormComponent } from './pages/form/form.component';
import { UsersComponent } from './pages/users/users.component';
//services
import { ServicesService } from './services/services.service';
//http module
import { HttpClientModule,HTTP_INTERCEPTORS } from '@angular/common/http'
//forms
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
//storage
import { StorageServiceModule } from 'ngx-webstorage-service';
import { CamelcasePipe } from './pipes/camelcase.pipe';
import { HeaderService } from './services/interceptors/header.service';
@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    HomeComponent,
    FormComponent,
    UsersComponent,
    CamelcasePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    StorageServiceModule
  ],
  providers: [ServicesService, 
		{
		  provide: HTTP_INTERCEPTORS,
		  useClass: HeaderService,
		  multi: true
		}],
  bootstrap: [AppComponent]
})
export class AppModule { }
